class readystock extends kue {

    private double jumlah;

    public readystock(String name, double price, double jumlah) {
        super(name, price); // memanggil attribute dari parent class
        setJumlah(jumlah);
    }

    public double getJumlah() {
        return jumlah;
    }

    public void setJumlah(double Jumlah) {
        jumlah = Jumlah;
    }

    // menghitung total harga dari kue pesanan
    public double hitungHarga() {
        return super.getPrice() * jumlah * 2;
    }

    // memanggil method dari parent class
    @Override
    public double Berat() {
        return 0;
    }

    @Override
    public double Jumlah() {
        return jumlah;
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(price);
    }
}